#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tcp350
#
require vac_ctrl_tcp350,1.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Tgt-MRV:Vac-VEPT-500
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEPT-500, IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4013")

#
# Device: Tgt-MRV:Vac-VPT-500
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = Tgt-MRV:Vac-VPT-500, CONTROLLERNAME = Tgt-MRV:Vac-VEPT-500")


#
# Device: Tgt-MRV:Vac-VEPT-600
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEPT-600, IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4014")

#
# Device: Tgt-MRV:Vac-VPT-600
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = Tgt-MRV:Vac-VPT-600, CONTROLLERNAME = Tgt-MRV:Vac-VEPT-600")


#
# Device: Tgt-MRV:Vac-VEPT-700
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEPT-700, IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4015")

#
# Device: Tgt-MRV:Vac-VPT-700
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = Tgt-MRV:Vac-VPT-700, CONTROLLERNAME = Tgt-MRV:Vac-VEPT-700")


#
# Device: Tgt-MRV:Vac-VEPT-800
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEPT-800, IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4016")

#
# Device: Tgt-MRV:Vac-VPT-800
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = Tgt-MRV:Vac-VPT-800, CONTROLLERNAME = Tgt-MRV:Vac-VEPT-800")


