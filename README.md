# IOC for RFQ vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   Tgt-MRV:Vac-VEPT-500
    *   Tgt-MRV:Vac-VPT-500
*   Tgt-MRV:Vac-VEPT-600
    *   Tgt-MRV:Vac-VPT-600
*   Tgt-MRV:Vac-VEPT-700
    *   Tgt-MRV:Vac-VPT-700
*   Tgt-MRV:Vac-VEPT-800
    *   Tgt-MRV:Vac-VPT-800